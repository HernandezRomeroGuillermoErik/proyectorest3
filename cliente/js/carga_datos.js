 function descargar(){ 
    var url="http://localhost/api/public/api/valores/anios";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var documento_xml = xhr.responseXML;
          organizar_datos(documento_xml);
        } else if (this.status === 403) {
            archivo_a_descargar.value = "Error: 403 Forbidden.";
        } else if (this.status === 404) {
            archivo_a_descargar.value = "Error: 404 Not Found.";
        } else {
            archivo_a_descargar.value = "Error: " + xhr.status;
        }
      }
    };
    xhr.open('GET', url);
    xhr.setRequestHeader("Accept", "application/xml");
    xhr.send();
  }

  function organizar_datos(documento_xml){
    values=new Array();
    valores=documento_xml.getElementsByTagName('valor');
    for (var i = 0; i < valores.length; i++) {
            evento=valores[i];
            anio=valores[i].children[0].innerHTML;
            dato=valores[i].children[1].innerHTML;
            console.log(dato+"  "+anio);

        }
  }
  
 function descargarEntidadAnios(id){ 
    var url="http://localhost/api/public/api/valores/entidades/"+id;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var documento_xml = xhr.responseXML;
          organizar_datosEntidadAnio(documento_xml);
        } else if (this.status === 403) {
            archivo_a_descargar.value = "Error: 403 Forbidden.";
        } else if (this.status === 404) {
            archivo_a_descargar.value = "Error: 404 Not Found.";
        } else {
            archivo_a_descargar.value = "Error: " + xhr.status;
        }
      }
    };
    xhr.open('GET', url);
    xhr.setRequestHeader("Accept", "application/xml");
    xhr.send();
  }

  function organizar_datosEntidadAnio(documento_xml){
    values=new Array();
    valores=documento_xml.getElementsByTagName('valor');
    for (var i = 0; i < valores.length; i++) {
            evento=valores[i];
            anio=valores[i].children[0].innerHTML;
            entidad=valores[i].children[1].innerHTML;
            dato=valores[i].children[2].innerHTML;
            console.log(anio+"  "+entidad+" "+dato);
            datoAnio[i]=dato;

        }
  }

  descargarEntidadAnios(1);