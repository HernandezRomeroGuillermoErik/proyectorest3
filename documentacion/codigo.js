function getEntidades(){
var elemento = document.getElementById('contenedor');

var o_h31=document.createElement('h3');
var texto1=document.createTextNode("obtEntidades");
o_h31.appendChild(texto1);
elemento.appendChild(o_h31);

var o_h41=document.createElement('h4');
var texto2=document.createTextNode("Descripción");
o_h41.appendChild(texto2);
elemento.appendChild(o_h41);

var o_par=document.createElement('p');
var texto3=document.createTextNode("Éste método obtiene todas las entidades registradas en la API");
o_par.appendChild(texto3);
elemento.appendChild(o_par);

var o_h42=document.createElement('h4');
var texto4=document.createTextNode("Ejemplo de la URI");
o_h42.appendChild(texto4);
elemento.appendChild(o_h42);

var o_a=document.createElement('a');
o_a.href="http://localhost/api/public/api/entidades";
elemento.appendChild(o_a);

var respuesta=document.createElement('h4');
var texto5=document.createTextNode("Ejemplo de  respuesta del servidor:");
respuesta.appendChild(texto5);
elemento.appendChild(respuesta);

var codigo1=document.createElement('pre');
var cod1=document.createTextNode(
&lt;?xml version="1.0" encoding="UTF-8"?&gt
&lt;catalog&gt
  &lt;entidad id="1"&gt
      &lt;descripcion&gtNacional&lt;/descripcion&gt
  &lt;/entidad&gt
  &lt;entidad id="2"&gt
      &lt;descripcion&gtAguasCalientes&lt;/descripcion&gt
  &lt;/entidad&gt
  &lt;entidad id="12"&gt
      &lt;descripcion&gtJalisco&lt;/descripcion&gt
  &lt;/entidad&gt
  &lt;entidad id="14"&gt
      &lt;descripcion&gtoaxaca&lt;/descripcion&gt
  &lt;/entidad&gt
&lt;/catalog&gt
);
codigo1.appendChild(cod1);
elemento.appendChild(codigo1);

var codigo2=document.createElement('pre');
var cod2=document.createTextNode(
{
       "entidades":
       [
           {
               "id": "1",
               "nombre": "Nacional"
           },
           {
               "id": "2",
               "nombre": "AguasCalientes"
           },
           {
               "id": "12",
               "nombre": "Jalisco"
           },
           {
               "id": "14",
               "nombre": "oaxaca"
           }
       ]
    }
);
codigo2.appendChild(cod2);
elemento.appendChild(codigo2);

}

cantidad=0;
function agregarEl(){ 
cantidad++;
elemento1 = document.createElement('input');
elemento1.type='text';
elemento1.size='50';
elemento1.name='respuesta'+cantidad;
elemento1.id ='respuesta'+cantidad;
elemento3 = document.createElement('h3');
elemento3.name='eti'+cantidad;
elemento3.value=cantidad;
elemento2 = document.getElementById('contenedor');
elemento2.appendChild(elemento3);
elemento2.appendChild(elemento1);
elemento2.appendChild(document.createElement('br'));
}

